Global Enum $eRender, $eCapture, $eAll, $EDataFlow_enum_count

Global Const $CLSCTX_INPROC_SERVER = 1
Global Const $DEVICE_STATE_ACTIVE = 0x00000001
Global Const $CLSID_MMDeviceEnumerator = "{BCDE0395-E52F-467C-8E3D-C4579291692E}"
Global Const $IID_IMMDeviceEnumerator = "{A95664D2-9614-4F35-A746-DE8DB63617E6}"
Global Const $tagIMMDeviceEnumerator = _
        "EnumAudioEndpoints hresult(int;dword;ptr*);" & _
        "GetDefaultAudioEndpoint hresult(int;int;ptr*);" & _
        "GetDevice hresult(wstr;ptr*);" & _
        "RegisterEndpointNotificationCallback hresult(ptr);" & _
        "UnregisterEndpointNotificationCallback hresult(ptr)"

Global Const $IID_IMMDeviceCollection = "{0BD7A1BE-7A1A-44DB-8397-CC5392387B5E}"
Global Const $tagIMMDeviceCollection = "GetCount hresult(uint*);Item hresult(uint;ptr*)"

Global Const $IID_IMMDevice = "{D666063F-1587-4E43-81F1-B948E807363F}"
Global Const $tagIMMDevice = _
        "Activate hresult(struct*;dword;ptr;ptr*);" & _
        "OpenPropertyStore hresult(dword;ptr*);" & _
        "GetId hresult(wstr*);" & _
        "GetState hresult(dword*)"

Global Const $IID_IAudioEndpointVolume = "{5CDF2C82-841E-4546-9722-0CF74078229A}"
Global Const $tagIAudioEndpointVolume = _
        "RegisterControlChangeNotify hresult(ptr);" & _
        "UnregisterControlChangeNotify hresult(ptr);" & _
        "GetChannelCount hresult(uint*);" & _
        "SetMasterVolumeLevel hresult(float;ptr);" & _
        "SetMasterVolumeLevelScalar hresult(float;ptr);" & _
        "GetMasterVolumeLevel hresult(float*);" & _
        "GetMasterVolumeLevelScalar hresult(float*);" & _
        "SetChannelVolumeLevel hresult(uint;float;ptr);" & _
        "SetChannelVolumeLevelScalar hresult(uint;float;ptr);" & _
        "GetChannelVolumeLevel hresult(uint;float*);" & _
        "GetChannelVolumeLevelScalar hresult(uint;float*);" & _
        "SetMute hresult(int;ptr);" & _
        "GetMute hresult(int*);" & _
        "GetVolumeStepInfo hresult(uint*;uint*);" & _
        "VolumeStepUp hresult(ptr);" & _
        "VolumeStepDown hresult(ptr);" & _
        "QueryHardwareSupport hresult(dword*);" & _
        "GetVolumeRange hresult(float*;float*;float*)"

HotKeySet("{PGDN}", "ToggleMute")

While 1
   Sleep(100)
WEnd

Func ToggleMute()
   Local $pDevice = 0
   Local $oMMDevice = 0
   Local $pEndpointVolume = 0
   Local $iStatus = 0
   Local $HRESULT = 0
   Local $bMute = False

   Local $oMMDeviceEnumerator = ObjCreateInterface($CLSID_MMDeviceEnumerator, $IID_IMMDeviceEnumerator, $tagIMMDeviceEnumerator)

   Local $pDeviceCollection = 0
   $oMMDeviceEnumerator.EnumAudioEndpoints($eCapture, $DEVICE_STATE_ACTIVE, $pDeviceCollection)

   Local $oIMMDeviceCollection = ObjCreateInterface($pDeviceCollection, $IID_IMMDeviceCollection, $tagIMMDeviceCollection)

   Local $iDeviceCount = 0
   $oIMMDeviceCollection.GetCount($iDeviceCount)

   For $i = 0 To $iDeviceCount - 1
	  $oIMMDeviceCollection.Item($i, $pDevice)
	  $oMMDevice = ObjCreateInterface($pDevice, $IID_IMMDevice, $tagIMMDevice)
	  $oMMDevice.Activate(__uuidof($IID_IAudioEndpointVolume), $CLSCTX_INPROC_SERVER, 0, $pEndpointVolume)
	  $oIAudioEndpointVolume = ObjCreateInterface($pEndpointVolume, $IID_IAudioEndpointVolume, $tagIAudioEndpointVolume)
	  $oIAudioEndpointVolume.GetMute($bMute)
	  $oIAudioEndpointVolume.SetMute(Not($bMute), Null)
	  $oMMDevice = 0
	  $oIAudioEndpointVolume = 0
   Next
   $oIMMDeviceCollection = 0
   $oMMDeviceEnumerator = 0

   If $bMute = False Then
	  SoundPlay("mic_muted.wav", 1)
   Else
	  SoundPlay("mic_activated.wav", 1)
   EndIf
EndFunc   ;==>ToggleMute

Func __uuidof($sGUID)
   Local $tGUID = DllStructCreate("ulong Data1;ushort Data2;ushort Data3;byte Data4[8]")
   DllCall("ole32.dll", "long", "CLSIDFromString", "wstr", $sGUID, "struct*", $tGUID)
   If @error Then Return SetError(@error, @extended, 0)
   Return $tGUID
EndFunc   ;==>__uuidof